/**
 * @file structure.h
 * @brief Header file containing structures and definitions for project management.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @def TMAXTRAV
 * @brief Maximum number of works (travaux) in the project.
 */
#define TMAXTRAV 8
#define TRACE false 

/**
 * @enum Booleen
 * @brief Boolean type definition.
 */
typedef enum {
    false, /**< Boolean value representing false. */
    true   /**< Boolean value representing true. */
} Booleen;

/**
 * @struct Devis
 * @brief Structure representing a quote or bid for a work.
 */
typedef struct {
    char *nomE;      /**< Name of the company. */
    char *adresse;   /**< Address of the company. */
    int capital;     /**< Capital of the company. */
    int duree;       /**< Duration of the work. */
    int prix;        /**< Price proposed for the work. */
} Devis;

/**
 * @struct MaillonDevis
 * @brief Node structure representing a quote in the list of quotes.
 */
typedef struct maillonDevis {
    Devis dev;                    /**< Information about the quote. */
    struct maillonDevis *suiv;    /**< Pointer to the next quote in the list. */
} MaillonDevis, *ListeDevis;

/**
 * @struct Offre
 * @brief Structure representing a work offer.
 */
typedef struct {
    char travaux[30];       /**< Type of work. */
    ListeDevis ldevis;      /**< List of quotes for the work. */
} Offre;

/**
 * @struct MaillonSucc
 * @brief Node structure representing a successor in the list of successors.
 */
typedef struct maillonSucc {
    char tache[20];                /**< Task name. */
    struct maillonSucc *nxt;       /**< Pointer to the next successor in the list. */
} MaillonSucc, *Liste;

/**
 * @struct Tache
 * @brief Structure representing a task in the project.
 */
typedef struct {
    char tache[20];         /**< Task name. */
    int duree;              /**< Duration of the task. */
    int nbPred;             /**< Number of predecessors. */
    Liste succ;             /**< List of successors. */
    int dateDebut;          /**< Start date of the task. */
    Booleen traite;         /**< Boolean flag indicating whether the task has been processed. */
} Tache;

/**
 * @struct Precedence
 * @brief Structure representing a precedence relationship between two works.
 */
typedef struct {
    char travauxPrec[30];   /**< Type of preceding work. */
    char travauxSucc[30];   /**< Type of succeeding work. */
} Precedence;

/**
 * @struct ListeAttente
 * @brief Structure representing a queue for tasks waiting to be processed.
 */
typedef struct fileAtt {
    Tache *tab[TMAXTRAV];   /**< Array to store tasks. */
    int debut;              /**< Front of the queue. */
    int fin;                /**< Rear of the queue. */
} ListeAttente;
