#include "structures.h"

void trace(char str[]);

// Chargements

char		*readAdress(FILE *flot);
Offre 		**initTabTravaux(void);
Offre 		**loadOffre(void);
void		loadDevis(FILE *devisFile, Offre **tabTravaux);
Tache		**loadTaches(Offre **tabTravaux);
Precedence	*loadPrec(int *tmax, int *tlog);


// Recherches et tri

int 	rechTravaux(char *travauxName, Offre **tabTravaux);
void	minTravaux(Offre **tabTravaux);
void 	freeListeDevis(ListeDevis *ldevis);
void	fusionDevisElements(ListeDevis *R, int *tlogR, ListeDevis *S, int *tlogS, ListeDevis *mergedList, ListeDevis *last, int *k);
void	gestionElementsRestants(ListeDevis *mergedList, ListeDevis *last, ListeDevis *source, int *tlogSource, int *k);
void	fusionMaillonDevis(ListeDevis R, int tlogR, ListeDevis S, int tlogS, ListeDevis *T);
void	triFusionListeDevis(ListeDevis *ldevis, int tlog);
void	fusion(Tache *R[], int tR, Tache *S[], int tS, Tache *T[]);
void	copy(Tache *T[], int i, int j, Tache *R[]);
void	triFusion(Tache *T[], int tlog);



// Fonctions de base -> Qui étaient bien dans structures.h...

int			max(int a, int b);
int			lenListeDevis(ListeDevis l);
int 		longueurMaxNomEntreprise(ListeDevis ldevis);
int			nombrePred(char travaux[], Precedence prec[], int tlog);
int 		calculerDureeProjet(Tache **tachesTriees, int nbTaches);
void		displayDevis(Devis d);
void		displayOffre(Offre *o);
void		displayListeDevis(ListeDevis l);
void 		afficherDevisEntreprise(Offre **tabTravaux);
void 		displayPrecedences(Precedence *tabP, int tlog);
void 		afficherTaches(Tache **tachesTriees, int p_tmax);
void		afficherTachesTriees(Tache *tachesTriees[], int p_tmax);
void 		enfiler(ListeAttente *file, Tache *tache);
Offre		*newOffre(char *travauxName);
Booleen		emptyListe(ListeDevis l);
Booleen		emptyOffre(Offre *o);
Booleen		estVide(ListeAttente *file);
Liste		newListe(void);
Liste		insertSucc(Liste l, char travaux[]);
Liste		lstSucc(char travaux[], Precedence prec[], int tlog);
ListeDevis	del(ListeDevis l);
ListeDevis	insert(ListeDevis l, Devis d);
ListeDevis	newListeDevis(void);
Tache		*trouverTache(Tache **tabTache, int nbTaches, char *nom);
Tache* defiler(ListeAttente *file);
ListeAttente* initialiserFileAttente(void);


void traiterTaches(Tache *tabTache[]);
