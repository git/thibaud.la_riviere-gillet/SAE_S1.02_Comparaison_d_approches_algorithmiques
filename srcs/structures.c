/**
 * @file structure.c
 * @brief Fichier contenant des fonctions liées à l'initialisation, la manipulation et l'affichage des structures de données.
 */

#include "../includes/main.h"

/**
 * @brief Initialise une nouvelle liste de devis.
 * @return Liste de devis initialisée.
 */
ListeDevis newListeDevis(void)
{
	trace("newListeDevis");
	return NULL;
}

/**
 * @brief Initialise une nouvelle offre.
 * @param travauxName Nom du travail associé à l'offre.
 * @return Offre initialisée.
 */
Offre *newOffre(char *travauxName)
{
	trace("newOffre");
	Offre *o;
	o = (Offre *)malloc(sizeof(Offre));
	strcpy(o->travaux, travauxName);
	o->ldevis=newListeDevis();
	return o;
}

/**
 * @brief Initialise une nouvelle liste pour les successeurs.
 * @return Liste initialisée.
 */
Liste newListe(void)
{
	trace("newListe");
	return NULL;
}

/**
 * @brief Initialise une file d'attente.
 * @return File d'attente initialisée.
 */
ListeAttente* initialiserFileAttente(void)
{
	ListeAttente *file;

    file = malloc(sizeof(ListeAttente));
    if (!file)
    {
        printf("Erreur d'allocation mémoire\n");
        exit(1);
    }

    file->debut = 0;
    file->fin = 0;

    return (file);
}

/**
 * @brief Vérifie si une liste de devis est vide.
 * @param l Liste de devis à vérifier.
 * @return True si la liste est vide, False sinon.
 */
Booleen emptyListe(ListeDevis l)
{
	trace("emptyListe");
	return l==NULL;
}

/**
 * @brief Vérifie si une offre est vide (sans devis associé).
 * @param o Offre à vérifier.
 * @return True si l'offre est vide, False sinon.
 */
Booleen emptyOffre(Offre *o)
{
	trace("emptyOffre");
	return emptyListe(o->ldevis);
}

/**
 * @brief Vérifie si une file d'attente est vide.
 * @param file File d'attente à vérifier.
 * @return True si la file est vide, False sinon.
 */
Booleen estVide(ListeAttente *file)
{
    return (file->debut == file->fin);
}


/**
 * @brief Insère un devis en tête de la liste des devis.
 * @param l Liste de devis.
 * @param d Devis à insérer.
 * @return Liste de devis mise à jour.
 */
ListeDevis insert(ListeDevis l, Devis d)
{
	trace("insert");
	MaillonDevis *md;

	md = (MaillonDevis *)malloc(sizeof(MaillonDevis));
	if (md == NULL)
	{
		printf("\033[0;31mErreur: \033[0mallocation mémoire insertion MaillonDevis\n");
		exit(1);
	}
	md->dev=d;
	md->suiv=l;

	return md;
}

/**
 * @brief Insère en queue de liste (pour les successeurs).
 * @param l Liste de successeurs.
 * @param travaux Nom du travail associé au successeur.
 * @return Liste de successeurs mise à jour.
 */
Liste insertSucc(Liste l, char travaux[])
{
    trace("insertSucc");
    MaillonSucc *ms;

    ms = (MaillonSucc *)malloc(sizeof(MaillonSucc));
    if (ms == NULL)
    {
        printf("\033[0;31mErreur: \033[0mmalloc liste successeurs\n");
        exit(1);
    }

    ms->nxt = l;
    l = ms;
    strcpy(ms->tache, travaux);

    return l; 
}

/**
 * @brief Ajoute une tâche à la file d'attente.
 * @param file File d'attente.
 * @param tache Tâche à ajouter.
 */
void enfiler(ListeAttente *file, Tache *tache)
{
    if ((file->fin + 1) % TMAXTRAV == file->debut)
    {
        printf("\033[0;31mErreur: \033[0mFile d'attente pleine\n");
        exit(1);
    }

    file->tab[file->fin] = tache;
    file->fin = (file->fin + 1) % TMAXTRAV;
}

/**
 * @brief Supprime le premier maillon de la liste de devis.
 * @param l Liste de devis.
 * @return Liste de devis mise à jour.
 */
ListeDevis del(ListeDevis l)
{
	trace("del");
    MaillonDevis *md;

    if (l == NULL)
    {
        printf("\033[0;31mErreur: \033[0msuppression dans une liste vide\n");
        exit(1);
    }
    md = l;
    l = l->suiv;
    if (md->suiv == NULL)
    {
        free(md->dev.nomE);
        free(md->dev.adresse);
        free(md);
        return newListeDevis();
    }

    free(md->dev.nomE);
    free(md->dev.adresse);
    free(md);
    return (l);
}

/**
 * @brief Défile une tâche de la file d'attente.
 * @param file File d'attente.
 * @return Tâche défilée.
 */
Tache* defiler(ListeAttente *file)
{
	Tache *tache;

    if (file->debut == file->fin)
        return NULL; 

    tache = file->tab[file->debut];
    file->debut = (file->debut + 1) % TMAXTRAV;

    return (tache);
}

/**
 * @brief Calcule la longueur d'une liste de devis.
 * @param l Liste de devis.
 * @return Longueur de la liste.
 */
int lenListeDevis(ListeDevis l)
{
	trace("lenListeDevis");
	int len=0;
	while (l)
	{
		len++;
		l=l->suiv;
	}
	return (len);
}

/**
 * @brief Calcule la longueur maximale du nom d'entreprise dans une liste de devis.
 * @param ldevis Liste de devis.
 * @return Longueur maximale du nom d'entreprise.
 */
int lenMaxNomEntreprise(ListeDevis ldevis)
{
	trace("lenMaxNomEntreprise");
	int maxLen;
	int len;

	maxLen = 0;
	while (ldevis) 
	{
		len = strlen(ldevis->dev.nomE);
		if (len > maxLen) 
			maxLen = len;
		ldevis = ldevis->suiv;
	}
	return (maxLen);
}

/**
 * @brief Calcule le nombre de précédences pour un travail donné.
 * @param travaux Nom du travail.
 * @param prec Tableau de précédences.
 * @param tlog Taille logique du tableau de précédences.
 * @return Nombre de précédences.
 */
int nombrePred(char travaux[], Precedence prec[], int tlog)
{
	trace("nombrePred");
	int nbPred = 0, i;
	for (i=0; i<tlog; i++)
		if (strcmp(travaux, prec[i].travauxSucc)==0)
			nbPred++;
	return nbPred;
}

/**
 * @brief Retourne le maximum entre deux entiers.
 * @param a Premier entier.
 * @param b Deuxième entier.
 * @return La valeur maximale entre a et b.
 */
int max(int a, int b)
{
    if (a > b)
        return a;
    else
        return b;
}

/**
 * @brief Calcule la durée totale du projet en fonction des tâches triées.
 * @param tachesTriees Tableau des tâches triées.
 * @param nbTaches Nombre de tâches dans le tableau.
 * @return La durée totale du projet.
 */
int calculerDureeProjet(Tache **tachesTriees, int nbTaches)
{
	int i;
    int dureeProjet;
    int finTache;

    dureeProjet = 0;
    for (i = 0; i < nbTaches; i++)
    {
        finTache = tachesTriees[i]->dateDebut + tachesTriees[i]->duree;
        if (finTache > dureeProjet)
            dureeProjet = finTache;
    }
    return (dureeProjet);
}

/**
 * @brief Retourne une liste des successeurs pour un travail donné.
 * @param travaux Nom du travail.
 * @param prec Tableau des précédences.
 * @param tlog Taille logique du tableau des précédences.
 * @return Liste des successeurs pour le travail donné.
 */
Liste lstSucc(char travaux[], Precedence prec[], int tlog)
{
	trace("lstSucc");
	int i;
	Liste succ;

	succ = newListe();
	for (i=0; i<tlog; i++)
		if(strcmp(travaux, prec[i].travauxPrec)==0)
			succ=insertSucc(succ, prec[i].travauxSucc);
	return succ;
}

/**
 * @brief Trouve une tâche par son nom dans un tableau de tâches.
 * @param tabTache Tableau de tâches.
 * @param nbTaches Nombre de tâches dans le tableau.
 * @param nom Nom de la tâche à trouver.
 * @return Pointeur vers la tâche trouvée, ou NULL si non trouvée.
 */
Tache* trouverTache(Tache **tabTache, int nbTaches, char *nom)
{
	int i;

	if (tabTache == NULL || nom == NULL)
        return NULL;

    for (i = 0; i < nbTaches; i++) 
        if (strcmp(tabTache[i]->tache, nom) == 0) 
            return tabTache[i];
    return (NULL);
}

/**
 * @brief Liste les tâches restantes à réaliser après une date donnée.
 * @param tachesTriees Tableau des tâches triées.
 * @param nbTaches Nombre de tâches dans le tableau.
 * @param date Date de référence.
 */
void listerTachesRestantes(Tache **tachesTriees, int nbTaches, int date)
{
    printf("Taches restantes a realiser :\n");
    for (int i = 0; i < nbTaches; i++)
        if (tachesTriees[i]->dateDebut >= date)
            printf("Tache: %s\n", tachesTriees[i]->tache);
}


/**
 * @brief Affiche les détails d'un devis.
 * @param d Devis à afficher.
 */
void displayDevis(Devis d)
{
	trace("displayDevis");
	printf("\t%s\t%s\t%d\t%d\t%d\n", d.nomE, d.adresse, d.capital, d.duree, d.prix);
}

/**
 * @brief Affiche les détails d'une offre, y compris sa liste de devis.
 * @param o Offre à afficher.
 */
void displayOffre(Offre *o)
{
	trace("displayOffre");
	printf("%s\n", o->travaux);
	displayListeDevis(o->ldevis);
	printf("\n");
}

/**
 * @brief Affiche les détails d'une liste de devis.
 * @param l Liste de devis à afficher.
 */
void displayListeDevis(ListeDevis l)
{
	trace("displayListeDevis");
	while(l != NULL)
	{
		displayDevis(l->dev);
		l=l->suiv;
	}
}

/**
 * @brief Affiche le devis d'une entreprise donnée pour un type de travaux donné.
 * @param tabTravaux Tableau des offres.
 */
void afficherDevisEntreprise(Offre **tabTravaux)
{
	trace("afficherDevisEntreprise");
	int indiceTravaux;
	int maxlen;
	char typeTravaux[30];
	char *nomEntreprise;

	printf("Saisir le type de travaux : ");
	fgets(typeTravaux, sizeof(typeTravaux), stdin);
	typeTravaux[strlen(typeTravaux) - 1] = '\0';

	indiceTravaux = rechTravaux(typeTravaux, tabTravaux);

	if (indiceTravaux == -1) 
	{
		printf("Type de travaux non trouvé.\n");
		return;
	}

	maxlen = lenMaxNomEntreprise(tabTravaux[indiceTravaux]->ldevis);
	nomEntreprise = malloc((maxlen + 1) * sizeof(char));
	printf("Saisir le nom de l'entreprise : ");
	fgets(nomEntreprise, maxlen + 1, stdin);

	ListeDevis listeDevis = tabTravaux[indiceTravaux]->ldevis;
	while (listeDevis != NULL) 
	{
		if (strcmp(listeDevis->dev.nomE, nomEntreprise) == 0) 
		{
			printf("Devis pour %s de l'entreprise %s :\n", typeTravaux, nomEntreprise);
			displayDevis(listeDevis->dev);
			return;
		}
		listeDevis = listeDevis->suiv;
	}

	printf("Devis non trouvé pour l'entreprise %s dans le type de travaux %s.\n", nomEntreprise, typeTravaux);
	free(nomEntreprise);
}

/**
 * @brief Affiche les précédences entre les travaux.
 * @param tabP Tableau des précédences.
 * @param tlog Taille logique du tableau des précédences.
 */
void displayPrecedences(Precedence *tabP, int tlog)
{
	trace("displayPrecedences");
	int i;

    printf("Liste des precedences :\n");
    for (i = 0; i < tlog; i++)
        printf("\t%s\t\t : \t%s\n", tabP[i].travauxPrec, tabP[i].travauxSucc);
}

/**
 * @brief Affiche les tâches triées par ordre d'exécution.
 * @param tachesTriees Tableau des tâches triées.
 * @param p_tmax Taille du tableau des tâches triées.
 */
void afficherTachesTriees(Tache *tachesTriees[], int p_tmax)
{
	int j;

    printf("Affichage par ordre d'exécution :\n");
    for (j = 0; j < p_tmax; j++)
        printf("Tâche %s : Date de début au plus tôt = %d\n", tachesTriees[j]->tache, tachesTriees[j]->dateDebut);
}