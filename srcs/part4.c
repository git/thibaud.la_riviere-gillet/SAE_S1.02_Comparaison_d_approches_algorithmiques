/**
 * @file part4.c
 * @brief Fichier contenant la fonction de traitement des tâches.
 */

#include "../includes/main.h"

/**
 * @brief Traite les tâches en utilisant la méthode du graphe topologique.
 * @param tabTache Tableau des tâches à traiter.
 */
void traiterTaches(Tache *tabTache[]) {
    // Initialisation de la file d'attente
    ListeAttente file;
    Liste succCourant;
    file.debut = 0;
    file.fin = 0;

    // Enfiler les tâches sans prédécesseurs
    for (int i = 0; i < TMAXTRAV; i++)
        if (tabTache[i]->nbPred == 0)
            enfiler(&file, tabTache[i]);

    // Traitement des tâches
    while (!estVide(&file))
    {
        // Récupération de la tâche actuelle
        Tache *tacheActuelle = defiler(&file);
        succCourant = tacheActuelle->succ;

        // Traitement des successeurs
        while (succCourant)
        {
            // Recherche du successeur dans le tableau des tâches
            Tache *successeur = trouverTache(tabTache, TMAXTRAV, succCourant->tache);

            // Mise à jour de la date de début du successeur et du nombre de prédécesseurs
            if (successeur != NULL)
            {
                successeur->dateDebut = max(successeur->dateDebut, tacheActuelle->dateDebut + tacheActuelle->duree);
                successeur->nbPred--;

                // Enfiler le successeur si le nombre de prédécesseurs atteint zéro
                if (successeur->nbPred == 0)
                    enfiler(&file, successeur);
            }

            succCourant = succCourant->nxt;
        }
    }
}
