/**
 * @file sort.c
 * @brief Fichier contenant des fonctions de recherche et de tri.
 */

#include "../includes/main.h"

/**
 * @brief Recherche une offre dans le tableau de travaux.
 * @param travauxName Nom du travail à rechercher.
 * @param tabTravaux Tableau des offres.
 * @return L'indice de l'offre dans le tableau si trouvée, sinon -1.
 */
int rechTravaux(char *travauxName, Offre **tabTravaux)
{
	trace("rechTravaux");
	//On utilise la taille max de travaux car travaux est un tableau constant, sa taille max est égale à sa taille logique.
	int deb=0, end=TMAXTRAV-1, mid;

	while(deb<=end)
	{
		mid = (deb+end) /2;
		if (strcmp(travauxName, tabTravaux[mid]->travaux)==0)
			return mid;
		if (strcmp(travauxName, tabTravaux[mid]->travaux) < 0)
			end=mid-1;
		else
			deb=mid+1;
	}
	return -1;
}

/**
 * @brief Recherche et garde la meilleure offre par travaux dans le tableau.
 * Affiche chaque offre gardée avec le devis ayant le prix le plus bas.
 * @param tabTravaux Tableau des offres.
 */
void minTravaux(Offre *tabTravaux[])
{
	trace("minTravaux");
    int i;
    MaillonDevis *minDev, *md, *s;
    for (i = 0; i < TMAXTRAV; i++)
    {
    	md = NULL;
        s = NULL;
        minDev = tabTravaux[i]->ldevis;
        if (md == NULL && minDev->suiv)
        	md = minDev->suiv;
        while (md)
        {
            if ((md->dev).prix < (minDev->dev).prix || 
			    (((md->dev).prix == (minDev->dev).prix) && ((md->dev).capital > (minDev->dev).capital)))
            {
            	s = minDev;
            	minDev = md;
            	md = md->suiv;
            }
            else if ((md->dev).prix == (minDev->dev).prix && ((md->dev).capital == (minDev->dev).capital)) // PAS UNE ERREUR 
            {
            	printf("deux offres minimales !\n");
            	exit (1);
            }
            else
            {
            	s = md;
            	md = md->suiv;
            }
            del(s);
        }
        minDev->suiv = NULL;
        tabTravaux[i]->ldevis = minDev;
        displayOffre(tabTravaux[i]);
    }
}

/**
 * @brief Libère la mémoire d'une liste de devis.
 * @param ldevis Liste de devis à libérer.
 */
void freeListeDevis(ListeDevis *ldevis)
{
	trace("freeListeDevis");
	ListeDevis current = *ldevis;
	ListeDevis next;

	while (current) 
	{
		next = current->suiv;
		free(current);
		current = next;
	}
	*ldevis = NULL;
}

/**
 * @brief Gère les éléments restants après les itérations principales de fusion.
 * @param mergedList Liste fusionnée.
 * @param last Dernier élément de la liste fusionnée.
 * @param source Liste source.
 * @param tlogSource Taille logique de la liste source.
 * @param k Compteur d'éléments.
 */
void gestionElementsRestants(ListeDevis *mergedList, ListeDevis *last, ListeDevis *source, int *tlogSource, int *k)
{
	trace("gestionElementsRestants");
	while (*tlogSource > 0)
	{
		if (!*mergedList)
			*mergedList = *last = (ListeDevis)malloc(sizeof(MaillonDevis));
		else
		{
			(*last)->suiv = (ListeDevis)malloc(sizeof(MaillonDevis));
			*last = (*last)->suiv;
		}
		(*last)->dev = (*source)->dev;
		*source = (*source)->suiv;
		(*last)->suiv = NULL;
		(*k)++;
		(*tlogSource)--;
	}
}

/**
 * @brief Fusionne deux listes de devis triées en une seule liste triée.
 * @param R Première liste de devis.
 * @param tlogR Taille logique de la première liste de devis.
 * @param S Seconde liste de devis.
 * @param tlogS Taille logique de la seconde liste de devis.
 * @param T Liste fusionnée.
 */
void fusionMaillonDevis(ListeDevis R, int tlogR, ListeDevis S, int tlogS, ListeDevis *T)
{
	trace("fusionMaillonDevis");
	ListeDevis	last;
	ListeDevis	mergedList;
	int		k;

	last = NULL;
	mergedList = NULL;
	k = 0;
	fusionDevisElements			(&R, &tlogR, &S, &tlogS, &mergedList, &last, &k);
	gestionElementsRestants			(&mergedList, &last, &R, &tlogR, &k);
	gestionElementsRestants			(&mergedList, &last, &S, &tlogS, &k);
	freeListeDevis(T);
	*T = mergedList;
}

/**
 * @brief Boucle principale de fusion de listes de devis.
 * @param R Première liste de devis.
 * @param tlogR Taille logique de la première liste de devis.
 * @param S Seconde liste de devis.
 * @param tlogS Taille logique de la seconde liste de devis.
 * @param mergedList Liste fusionnée.
 * @param last Dernier élément de la liste fusionnée.
 * @param k Compteur d'éléments.
 */
void fusionDevisElements(ListeDevis *R, int *tlogR, ListeDevis *S, int *tlogS, ListeDevis *mergedList, ListeDevis *last, int *k)
{
	trace("fusionDevisElements");
	ListeDevis	*current;

	while (*tlogR > 0 && *tlogS > 0)
	{
		if (strcmp((*R)->dev.nomE, (*S)->dev.nomE) < 0)
		{
			current = R;
			(*tlogR)--;
		}
		else
		{
			current = S;
			(*tlogS)--;
		}
		if (!(*mergedList))
		{
			*mergedList = (ListeDevis)malloc(sizeof(MaillonDevis));
			*last = *mergedList;
		}
		else
		{
			(*last)->suiv = (ListeDevis)malloc(sizeof(MaillonDevis));
			*last = (*last)->suiv;
		}
		(*last)->dev = (*current)->dev;
		*current = (*current)->suiv;
		(*last)->suiv = NULL;
		(*k)++;
	}
}

/**
 * @brief Trie une liste de devis en utilisant l'algorithme de tri fusion.
 * @param ldevis Liste de devis à trier.
 * @param tlog Taille logique de la liste de devis.
 */
void triFusionListeDevis(ListeDevis *ldevis, int tlog)
{
	trace("triFusionListeDevis");
	ListeDevis	p;
	ListeDevis	q;
	ListeDevis	qTemp;

	p = *ldevis;
	q = p->suiv;
	if (tlog <= 1)
		return;
	for (int i = 0; i < tlog / 2 - 1 && q && q->suiv; i++)
	{
		p = p->suiv;
		q = q->suiv->suiv;
	}
	qTemp = p->suiv;
	p->suiv = NULL;
	triFusionListeDevis(ldevis, tlog / 2);
	triFusionListeDevis(&qTemp, tlog - tlog / 2);
	ListeDevis mergedList = NULL;
	fusionMaillonDevis(*ldevis, tlog / 2, qTemp, tlog - tlog / 2, &mergedList);
	*ldevis = mergedList;
}

/**
 * @brief Fusionne deux tableaux de tâches triés en un tableau trié.
 * @param R Premier tableau de tâches.
 * @param tR Taille du premier tableau de tâches.
 * @param S Second tableau de tâches.
 * @param tS Taille du second tableau de tâches.
 * @param T Tableau fusionné.
 */
void fusion(Tache *R[], int tR, Tache *S[], int tS, Tache *T[])
{
    int i, j, k;
    i = 0;
    j = 0;
    k = 0;

    while (i < tR && j < tS)
    {
        if (R[i]->dateDebut < S[j]->dateDebut)
        {
            T[k] = R[i];
            i++;
        }
        else
        {
            T[k] = S[j];
            j++;
        }
        k++;
    }

    while (i < tR)
    {
        T[k] = R[i];
        i++;
        k++;
    }

    while (j < tS)
    {
        T[k] = S[j];
        j++;
        k++;
    }
}

/**
 * @brief Copie les éléments d'un tableau dans un autre.
 * @param T Tableau source.
 * @param i Indice de début de copie.
 * @param j Indice de fin de copie.
 * @param R Tableau de destination.
 */
void copy(Tache *T[], int i, int j, Tache *R[])
{
    int k;

    k = 0;
    while (i < j)
    {
        R[k] = T[i];
        i++;
        k++;
    }
}

/**
 * @brief Trie un tableau de tâches en utilisant l'algorithme de tri fusion.
 * @param T Tableau de tâches à trier.
 * @param tlog Taille logique du tableau de tâches.
 */
void triFusion(Tache *T[], int tlog)
{
    if (tlog <= 1)
        return;

    Tache **R = (Tache **)malloc(sizeof(Tache *) * (tlog / 2));
    Tache **S = (Tache **)malloc(sizeof(Tache *) * (tlog - tlog / 2));

    if (!R || !S)
        exit(1);

    copy(T, 0, tlog / 2, R);
    copy(T, tlog / 2, tlog, S);

    triFusion(R, tlog / 2);
    triFusion(S, tlog - tlog / 2);
    fusion(R, tlog / 2, S, tlog - tlog / 2, T);

    free(R);
    free(S);
}