/**
 * @file charge.c
 * @brief Contient les fonctions liées à la gestion des charges, offres, devis, précédences, et tâches.
 */

#include "../includes/main.h"

/**
 * @brief Initialise le tableau des travaux.
 * @return Un tableau d'offres initialisées.
 */
Offre **initTabTravaux(void)
{
    trace("initTabTravaux");
    Offre **tabTravaux; 
    
    tabTravaux = (Offre **)malloc(TMAXTRAV * sizeof(Offre *));
    if (tabTravaux == NULL)
    {
        printf("\033[0;31mErreur: \033[0mmalloc tableau tabTravaux\n");
        exit(1);
    }

    // Initialisation de chaque élément du tableau
    tabTravaux[0] = newOffre("Electricite");
    tabTravaux[1] = newOffre("Finitions");
    tabTravaux[2] = newOffre("Fondation");
    tabTravaux[3] = newOffre("Murs");
    tabTravaux[4] = newOffre("Peinture");
    tabTravaux[5] = newOffre("Plomberie");
    tabTravaux[6] = newOffre("Sols");
    tabTravaux[7] = newOffre("Structure");

    return tabTravaux;
}

/**
 * @brief Charge une chaîne de caractères depuis un fichier.
 * @param file Pointeur vers le fichier.
 * @return La chaîne de caractères lue depuis le fichier.
 */
char *readFromFile(FILE *file)
{
    trace("readFromFile");
    char	*str;
    char	*buf;
    int	len = 0;
    int	size = 5;
    int	add = 5;

    str = (char *)malloc(size * sizeof(char));
    if (str == NULL)
        exit(1);

    while (str[len] != '\n' && !feof(file))
    {
        len = strlen(str);
        if (len >= size - 1)
        {
            size += add;
            buf = realloc(str, size);
            if (buf == NULL)
            {
                free(str);
                exit(1);
            }
            str = buf;
        }
        fgets(str + len, size - len, file);
        len = strlen(str) - 1;
    }
    str[len] = '\0';
    return (str);
}

/**
 * @brief Charge un devis depuis un fichier.
 * @param devisFile Pointeur vers le fichier de devis.
 * @param tabTravaux Tableau des offres.
 */
void loadDevis(FILE *devisFile, Offre **tabTravaux)
{
    trace("loadDevis");
    int	index;
    char	*travaux;
    Devis	d;

    travaux = readFromFile(devisFile);
    index = rechTravaux(travaux, tabTravaux);
    if (index == -1)
        exit(1);
    d.nomE = readFromFile(devisFile);
    d.adresse = readFromFile(devisFile);
    fscanf(devisFile, "%d\n%d\n%d%*c", &d.capital, &d.duree, &d.prix);
    tabTravaux[index]->ldevis = insert(tabTravaux[index]->ldevis, d);
}

/**
 * @brief Charge les offres depuis le fichier devis.txt.
 * @return Le tableau des offres chargées.
 */
Offre **loadOffre(void)
{
    trace("loadOffre");
    Offre	**tabTravaux = initTabTravaux();

    FILE	*devisFile;

    devisFile = fopen("files/devis.txt", "r");
    if (devisFile == NULL) //ERREUR
    {
        printf("\033[0;31mErreur: \033[0mouverture devis.txt");
        exit(1);
    }

    while (!feof(devisFile))
        loadDevis(devisFile, tabTravaux);
    for (int i = 0; i < TMAXTRAV; i++)
        triFusionListeDevis(&tabTravaux[i]->ldevis, lenListeDevis(tabTravaux[i]->ldevis));

    return (tabTravaux);
}

/**
 * @brief Charge les précédences depuis un fichier.
 * @param tmax Pointeur vers le nombre maximum de précédences.
 * @param tlog Pointeur vers le nombre actuel de précédences.
 * @return Un tableau de précédences.
 */
Precedence *loadPrec(int *tmax, int *tlog)
{
    trace("loadPrec");
	FILE *prec;
	Precedence *tabP, *s;
    char *precSuccStr;
    int index;

	prec=fopen("files/precedences.txt", "r");
    (*tmax)=5;
    (*tlog)=0;
	if (!prec)
	{
		printf("\033[0;31mErreur: \033[0mouverture de 'precedence.txt'\n");
		exit(1);
	}
	tabP = (Precedence *)malloc((*tmax) * sizeof(Precedence));
    if (!tabP)
    {
        printf("\033[0;31mErreur: \033[0mmalloc loadPrec\n");
        exit(1);
    }
    precSuccStr = readFromFile(prec);
	while(!feof(prec))
	{
        index=0;
		if((*tlog)==(*tmax))
		{
			(*tmax)+=5;
			s = (Precedence *)realloc(tabP, (*tmax) * sizeof(Precedence));
			if (!s)
			{
				printf("\033[0;31mErreur: \033[0mreallocation pour fichier précédence\n");
				exit(1);
			}
			tabP = s;
		}
        while (precSuccStr[index] != '\t')
            index++;
        strcpy(tabP[*tlog].travauxSucc, precSuccStr+index+1);
        precSuccStr[index]='\0';
        strcpy(tabP[*tlog].travauxPrec, precSuccStr);
        precSuccStr = readFromFile(prec);
		(*tlog)++;
	}
    free(precSuccStr);
	fclose(prec);
	return tabP;
}

/**
 * @brief Charge les tâches depuis un fichier pour chaque offre.
 * @param tabTravaux Tableau des offres.
 * @return Le tableau des tâches chargées.
 */
Tache **loadTaches(Offre *tabTravaux[])
{
    trace("loadTaches");
	Precedence *prec;
	int i, tmaxPrec, tlogPrec;
	Tache *t, **tabTache;

	tabTache = (Tache **)malloc(TMAXTRAV*sizeof(Tache *));
	if (!tabTache)
	{
		printf("\033[0;31mErreur: \033[0mmalloc tableau tache\n");
		exit(1);
	}
	for (i=0; i<TMAXTRAV; i++)
	{
		t=(Tache *)malloc(sizeof(Tache));
		if (!t)
		{
			printf("\033[0;31mErreur: \033[0mmalloc tache\n");
			exit(1);
		}

		strcpy(t->tache, tabTravaux[i]->travaux);
		t->duree = ((tabTravaux[i]->ldevis)->dev).duree;

		prec = loadPrec(&tmaxPrec, &tlogPrec);

		t->nbPred = nombrePred(tabTravaux[i]->travaux, prec, tlogPrec);
		t->succ = 	lstSucc(tabTravaux[i]->travaux, prec, tlogPrec);

		t->dateDebut=0;
		t->traite=0;

		tabTache[i] = t; 
	}
	return tabTache;
}