/**
 * @file main.c
 * @brief Fichier principal du programme, contenant la fonction main et la fonction de traçage.
 */

#include "../includes/main.h"

/**
 * @brief Fonction principale du programme.
 * @return 0 en cas de succès.
 */
int main() {
    // Initialisation des variables
	Precedence *tabP;
	Offre **tabTravaux;
	Tache **tabTaches;
	int p_tmax;
	int p_tlog;

    // Chargement des offres
	tabTravaux = loadOffre();

    // Traitement minimal des travaux
	minTravaux(tabTravaux);

    // Chargement des précédences
	tabP = loadPrec(&p_tmax, &p_tlog);

    // Affichage des précédences
	displayPrecedences(tabP, p_tlog);

    // Chargement des tâches
	tabTaches = loadTaches(tabTravaux);

    // Traitement des tâches
	traiterTaches(tabTaches);

    // Tri fusion des tâches
	triFusion(tabTaches, TMAXTRAV);

    // Affichage des tâches triées
	afficherTachesTriees(tabTaches, TMAXTRAV);

	return 0;
}

/**
 * @brief Affiche une trace si la constante TRACE est définie.
 * @param str Chaîne de caractères à afficher en tant que trace.
 */
void trace(char str[]) {
    // Vérification de la constante TRACE
	if(TRACE)
		printf("\n*-*-*  %s  *-*-*\n", str);
}
