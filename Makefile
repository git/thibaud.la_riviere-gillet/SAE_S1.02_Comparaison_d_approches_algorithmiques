NAME = SAE2 

RM = rm -rf
CC = gcc
CFLAGS = -Wall -Wextra

SRC_DIR = srcs
OBJ_DIR = obj

SRC_FILES =	$(SRC_DIR)/structures.c \
		$(SRC_DIR)/charge.c \
		$(SRC_DIR)/sort.c \
		$(SRC_DIR)/part4.c \
		$(SRC_DIR)/main.c

OBJ_FILES = $(SRC_FILES:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

all: $(NAME)

$(NAME): $(OBJ_FILES)
	$(CC) $(CFLAGS) $^ -o $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c | $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

clean:
	$(RM) $(OBJ_DIR)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
