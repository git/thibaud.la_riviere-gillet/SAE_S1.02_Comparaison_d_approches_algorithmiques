#include "../includes/main.h"


//test tri fusion
int	main(void)
{
	Offre offre1 = {"Travaux1", NULL};
	offre1.ldevis = (ListeDevis)malloc(sizeof(struct maillonDevis));
	offre1.ldevis->dev = (Devis){"Devis6", "Adresse1", 100, 5};
	offre1.ldevis->suiv = (ListeDevis)malloc(sizeof(struct maillonDevis));
	offre1.ldevis->suiv->dev = (Devis){"Devis3", "Adresse3", 80, 3};
	offre1.ldevis->suiv->suiv = (ListeDevis)malloc(sizeof(struct maillonDevis));
	offre1.ldevis->suiv->suiv->dev = (Devis){"Devis8", "Adresse2", 120, 7};
	offre1.ldevis->suiv->suiv->suiv = (ListeDevis)malloc(sizeof(struct maillonDevis));
	offre1.ldevis->suiv->suiv->suiv->dev = (Devis){"Devis5", "Adresse5", 90, 4};
	offre1.ldevis->suiv->suiv->suiv->suiv = (ListeDevis)malloc(sizeof(struct maillonDevis));
	offre1.ldevis->suiv->suiv->suiv->suiv->dev = (Devis){"Devis2", "Adresse2", 110, 6};
	offre1.ldevis->suiv->suiv->suiv->suiv->suiv = NULL;
	int tlog = 5;

	printf("Avant le tri : \n");
	displayOffre(&offre1);

	triFusionListeDevis(&offre1.ldevis, tlog);

	printf("\nApres le tri : \n");
	displayOffre(&offre1);

	freeListeDevis(&offre1.ldevis);

	return (0);
}
